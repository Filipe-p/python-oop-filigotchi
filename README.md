# Filigotchi OOP - Exercise

This code along is with regard to implementing OOP.

### Agenda

- OOP Basics (4 pillars)
- Write some user stories 
- Make a drawring of our classes, their attributes and behaviours(methods)
- Implemente the 4 pillars
- Use other python to make into a game

also, lets follow some best practices:

- no printing in methods
- keep code DRY
- seperation of concerns
- [extra] - error handling & testing

### OOP Basics

- Abstration - This happen with functions as well. it's just the ability to hide complex information. You achive this with inheritance, methods and good naming conventions. 
- Inheritance - When a subclass can create object that aquire all or most of the perant behaviors(methods) and attributes
- Encapsulation - Where you control the access to methods and attributes making them private or public. When Private only internal methods to the class can access this method.
- Polymrphism - Usually refers to method or inheritance polymorphism. Where a method will have one behaviour in the perante class, and have another with the child class. You do this by overiding a method completly, or by oriding and calling **super()**, to go feth the perant method and then run the new commmands

### User stories

As a player, I want to be able to create a Filigothci with name, so that I can then play the game.

As a Player, I want my filigotchi to have a health value that changes based on activity. (**encapsulation**)

As a Player, I want my filigotchi to a happiness value that changes based on entertaining activities and not being hungry. (**encapsulation**)

As a Player I want my filigotchi to have a fur type, and hights and number of eyes, so that I can imagine it. This need to be optional at start (**method polymorphism**)

As a Player, I want my filigotchi to play, so that it can be happy.

As a Player, I want my filigotchi to eat, so that it can be gain health.

As a Player, I want my filigotchi to go_potty, so it can be happy.

[extra] As a Player, when my filigotchi eats a status, go potty, showed become available.

[extra] As a Player, when the go potty status is anables, a filigotchi show be unhappy.

[extra] As a Player, I want my filigotchi to go to the farm up state when it has not more health. 

As a player we should be able to make water_filigotchi's that that have different method and heath and other properties. (**inheritance & Polymorphism**)

As a player, water_filigotchi's will have all the same atributes plus water_speed. **inheritance**

As a player, a water_filigotchi's will have all methods to play, eat, go_potty. **inheritance**

As a player the water_filigotchi's, play method will consume 5 more energy points than on land. **iheritance/method polymorphism** the same method, behaves differently

As a player the water_filigotchi's will have a new method called swim that will consume energy and make the water_filigotchi's happy. **iheritance polymorphism** making a new mmethod for the sub class.

### Link to Drawing

### Using our Filigotchi