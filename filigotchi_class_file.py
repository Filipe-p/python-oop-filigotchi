# define classes.
## Do not initiallise object here

class Filigotchi:

    #define the characteristis 
    def __init__(self, name, color='grey', fur_type='long fur', height="average hight"):
        self.name = name
        # Low level encapsulation where "hide" the characteristic or metho)
        ## useless
        self._health = 100
        # Actual encapsulation 
        self.__happiness = 50
        self.color = color
        self.fur_type = fur_type
        self.height = height

    ## Health related method
    def health_get(self):
        return self._health
    
    def __health_increase(self, number):
        self._health += number
        return self._health

    def __health_decrease(self, number):
        self._health -= number
        return self._health

    ## Happiness related methods
    def happiness_get(self):
        return self.__happiness

    def __happiness_increase(self, number):
        self._happiness = self._happiness + number
        return self._happiness

    def __happiness_decrease(self, number):
        self._happiness -= number
        return self._happiness

    # As a Player, I want my filigotchi to eat, so that it can be gain health. how much health gain and what to return 
    def eat(self):
        self.__health_increase(15)
        return "noom, nOOMmn NOOMM!"

    # As a Player, I want my filigotchi to play, so that it can be happy.
    def play(self):
        self.__happiness_increase(10)
        self.__health_decrease(5)
        return "so happy!"

    # As a Player, I want my filigotchi to go_potty, so it can be happy.
    def go_potty(self):
        self.__happiness_increase(15)
        self.__health_decrease(5)
        return ".... 0.0 ... 0.o  ...HUMMM!! Ahh I feel better"
    



class water_filigotchi(Filigotchi):

    def __init__(self, name, color='grey', fur_type='long fur', height="average hight", scale="lots"):
        super().__init__(name, color=color, fur_type=fur_type, height=height)
        scales = scales

    # super allows you to call the previous class methos

    # when you call a method on a sub class, 1st it looks to see if the method exist in the sub class.
    # 2nd if it does, it run that method, else it run the parent method. 
    # so when if we polymorth you actually complelty re write the method because it doesn't check the perant's method
    # to make it use the perants method, use super
