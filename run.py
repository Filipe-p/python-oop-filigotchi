# Here you call the classes and run code
# make instance here
from filigotchi_class_file import *

# As a player, I want to be able to create a Filigothci with name, so that I can then play the game.

filigotchi_a = Filigotchi('filipe')
print(filigotchi_a) #indeed an object of type <filigotchi_class_file.Filigotchi
print(filigotchi_a.name) # name set to object
print(filigotchi_a.fur_type)

#As a Player, I want my filigotchi to have a health value that changes based on activity. (encapsulation)
#As a Player, I want my filigotchi to a happiness value that changes based on entertaining activities and not being hungry. (encapsulation)
print(filigotchi_a._health) 
filigotchi_a._health = 999999 #WE HAKer MAN!
print(filigotchi_a._health) # PANIK
# The health is too open. We should not be able to read it directly or change it
# Still too open 

# print(filigotchi_a.__happiness) 
# filigotchi_a.__happiness = 999999 #WE stop HAKer MAN! 
# print(filigotchi_a.__happiness) # PANIK no happiness :(
# The health is too open. We should not be able to read it directly or change it

## As a Player I want my filigotchi to have a fur type, and hights and number of eyes, so that I can imagine it. This need to be optional at start (method polymorphism)
filigotchi_with_set_fur = Filigotchi('Ijaz', fur_type="long_hair", height="very short")
print(filigotchi_with_set_fur.name)
print(filigotchi_with_set_fur.fur_type)
print(filigotchi_with_set_fur.health_get())

filigotchi_with_set_fur.eat()
print(filigotchi_with_set_fur.health_get())


